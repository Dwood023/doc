pub struct Cursor<'a> {
    src: &'a str,
    i: usize
}

impl<'a> Cursor<'a> {
    pub fn new(src: &'a str) -> Cursor<'a> {
        Cursor {
            src,
            i: 0
        }
    }
    pub fn take_one(&mut self) -> &'a str {
        self.increment(1);
        &self.src[self.i - 1..self.i]
    }
    pub fn detect(&mut self, rgx: &regex::Regex) -> Option<regex::Captures<'a>> {
        let length_of_capture = |m: &regex::Captures| m.get(0).unwrap().end();

        if let Some(cap) = rgx.captures(self.remaining()) {
            self.increment(length_of_capture(&cap));
            Some(cap)
        } else {
            None
        }
    }
    pub fn is_empty(&self) -> bool {
        self.i >= self.src.len()
    }
    fn remaining(&self) -> &'a str {
        &self.src[self.i..]
    }
    fn increment(&mut self, n: usize) {
        self.i += n;
    }
}