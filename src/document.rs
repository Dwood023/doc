use line::Content;
use lexer::identify;
use node::Node;
use parser::Parser;
use footnotes::Footnotes;
use node::Tag::{Body, Div};
use metadata::Metadata;

// life flashes before your eyes and for a while you believe you might not have died at all
// Everyone is dying continuously throughout their whole life
// The self is constantly dying, and leaving offspring 

pub struct Document {
    pub metadata: Option<Metadata>,
    pub body: Vec<Node>,
    pub footnotes: Footnotes
}
impl Document {
    pub fn to_html(self) -> String {
        let mut children = if let Some(metadata) = self.metadata {
            vec!(metadata.to_html())
        } else {
            vec!()
        };

        children.push(
            Node::Parent {
                tag: Div {classes: Some("content".to_owned())},
                children: self.body
            }
        );

        children.push(
            self.footnotes.to_html()
        );

        Node::Parent {
            tag: Body,
            children
        }.serialize()
    }
    pub fn new(src: String) -> Document {

        let lexed: Vec<Option<Content>> = src.lines()
            .map(|l| identify(l))
            .collect();

        let mut parser = Parser::new();

        parser.parse_document(lexed)

    }
}