use node::Node::{self, Parent};
use parser::Parser;
use node::Tag::*;

pub struct Footnotes {
    count: usize,
    bibliography: Vec<Node>
}

impl Footnotes {
    pub fn new() -> Footnotes {
        Footnotes {
            count: 0,
            bibliography: vec!(
                // Hahaha holy shit this is obviously broke
                // Or is it...? Is it worth representing self-closing tags at the Node struct level?
                // Do we escape all text input to Leafs, or just escape source document text?
                Node::Leaf("<hr>".to_owned())
            )
        }
    }
    pub fn to_html(self) -> Node {
        Parent {
            tag: Div { classes: Some("footnotes".to_owned()) },
            children: self.bibliography
        }
    }
    pub fn generate_footnote(&mut self, content: &str) -> Node {
        self.count += 1;
        self.bibliography.push(
            Footnotes::bib_entry(self.count, content)
        );
        Footnotes::inline_ref(self.count)
    }
    fn get_inline_id(n: usize) -> String {
        format!("REF_inline_{}", n)
    }
    fn get_bib_id(n: usize) -> String {
        format!("REF_bib_{}", n)
    }

    fn bib_entry(n: usize, content: &str) -> Node {

        let marker = Parent {
            tag: Link { 
                to: Footnotes::get_inline_id(n),
                from: Footnotes::get_bib_id(n)
            },
            children: vec!(
                Node::Leaf(
                    format!("{}. ", n)
                )
            )
        };
        let mut parser = Parser::new();

        Node::Parent {
            tag: Div { classes: None },
            children: 
                std::iter::once(marker)
                .chain(
                    parser.inline_parse(content)
                ).collect()
        }
        // TODO - Nested footnotes - Could append footnotes of this local Parser to self.footnotes
    }
    fn inline_ref(n: usize) -> Node {
        Node::Parent {
            tag: Superscript,
            children: vec!(
                Node::Parent {
                    tag: Link { 
                        to: Footnotes::get_bib_id(n),
                        from: Footnotes::get_inline_id(n)
                    },
                    children: vec!(Node::Leaf(n.to_string()))
                }
            )
        }
    }
}