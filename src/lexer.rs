use regex::Regex;
use line::{Content::{self, *}, ListType};
use itertools::Itertools;

pub fn identify(line: &str) -> Option<Content> {

    lazy_static! {
        static ref OLI: Regex = Regex::new(r"^\d?\. (?P<content>.*)").unwrap();
        static ref DIV: Regex = Regex::new(r"^\[(?P<content>.*)\]").unwrap();
        static ref DIV_CLASS: Regex = Regex::new(r"\.(\w*)").unwrap();
        static ref DIV_ID: Regex = Regex::new(r"\#(\w*)").unwrap();
        static ref P: Regex = Regex::new(r"^(?P<content>.+)").unwrap();
        static ref H: Regex = Regex::new(r"^(?P<lvl>=+) (?P<content>.*)").unwrap();
        static ref ULI: Regex = Regex::new(r"^- (?P<content>.*)").unwrap();
    }

    type Constructor = Box<Fn(&str, usize) -> Option<Content>>;

    fn get_content<'a>(r: &Regex, line: &'a str) -> Option<&'a str> {
        r.captures(line).map( |cap|
            cap.name("content").unwrap().as_str()
        )
    }

    let paragraph: Constructor = Box::new(
        |line, indents| get_content(&P, line).map(
            |content| Paragraph {
                content,
                indents
            }
        )
    );
    let div: Constructor = Box::new(
        |line, indents| get_content(&DIV, line).map( |content| {
            let classes = DIV_CLASS.captures(content).unwrap()
                .iter()   
                .skip(1)
                .map(|c| c.unwrap().as_str())
                .join(" ");
            // TODO -- Div id
            Div {
                    classes: classes,
                    indents
            }
        })
    );
    let olist_item: Constructor = Box::new(
        |line, indents| get_content(&OLI, line).map( |content| ListItem {
            variant: ListType::Ordered,
            content,
            indents
        })
    );
    let ulist_item: Constructor = Box::new(
        |line, indents| get_content(&ULI, line).map( |content| ListItem {
            variant: ListType::Unordered,
            content,
            indents
        })
    );
    let header: Constructor = Box::new(
        |line, _| H.captures(line).map( |mat| {
            let content = mat.name("content").unwrap().as_str();
            let level = mat.name("lvl").unwrap().as_str().len();
            return Header {
                level,
                content
            }
        })
    );

    let ctors = [
        div,
        olist_item,
        ulist_item,
        header,
        paragraph // PARAGRAPH SHOULD COME LAST 
    ];

    let indents: String = line.chars()
        .take_while(|c| c.is_whitespace()).collect();
    let line: &str = &line[indents.len()..];
    let indents = get_indent(&indents);

    for ctor in ctors.iter() {
        let result = ctor(line, indents);
        if result.is_some() {
            return result
        }
    }

    None
}
fn get_indent(line: &str) -> usize {
	let mut tabs = 0;
    let mut spaces = 0;

	const TAB_WIDTH: usize =  4;

    for c in line.chars() {
		if c == '\t' {
			tabs += 1;
        }
		else if c == ' ' {
			spaces += 1;
        }
		else {
            break;
        }
	}

    return spaces + (tabs * TAB_WIDTH);
}