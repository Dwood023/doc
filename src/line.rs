use self::Content::*;
use node::Tag;
#[derive(Debug, Clone, PartialEq)]
pub enum Content<'a> {
    Header {
        content: &'a str,
        level: usize
    },
    ListItem {
        content: &'a str,
        indents: usize,
        variant: ListType
    },
    Paragraph {
        content: &'a str,
        indents: usize,
    },
    Div {
        indents: usize,
        classes: String
    }
}

impl<'a> Content<'a> {
    pub fn get_tag(&self) -> Tag {
        match self {
            Paragraph{..} => Tag::Paragraph,
            Header{level, ..} => Tag::Header { level: *level },
            ListItem{..} => Tag::ListItem,
            Div { classes, .. } => Tag::Div { classes: Some(classes.to_string()) }
        }
    }
    pub fn is_parent_of(&self, child: &Content) -> bool {
        match self {
            // CAN'T PARENT
            Header{..} | 
            Paragraph{..} 
                => false, 

            // PARENT ACCORDING TO INDENTATION
            ListItem { indents: parent_indents, .. } |
            Div {indents: parent_indents, .. } 
                => match child {
                    ListItem { indents, .. } |
                    Paragraph { indents, .. } |
                    Div {indents, ..} 
                        => parent_indents < indents,
                    Header {..} 
                        => false
                }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ListType {
    Ordered,
    Unordered
}

impl ListType {
    pub fn get_list_tag(&self) -> Tag {
        match self {
            ListType::Ordered => Tag::OrderedList,
            ListType::Unordered => Tag::UnorderedList
        }
    }
}
