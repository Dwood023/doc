extern crate regex;
#[macro_use] extern crate lazy_static;
extern crate itertools;
extern crate image_base64;
extern crate clap;
extern crate notify;
extern crate htmlescape;

pub mod utils;
pub mod node;
pub mod fs;
pub mod line;
pub mod lexer;
pub mod parser;
pub mod document;
pub mod footnotes;
pub mod cursor;
pub mod metadata;

use clap::{App, Arg};
use notify::{Watcher, watcher};
use std::sync::mpsc::channel;

use document::Document;

fn main() {
    let matches = App::new("Custom Doc (Working title)")
            .version("v0.0.1")
            .author("David Wood <dwood023@live.co.uk>")
            .about("Plaintext document format similiar to asciidoc")
            .arg(
                Arg::with_name("INPUT_FILE")
                    .help("Input file for conversion")
                    .required(true)
                    .index(1)
            )
            .arg(
                Arg::with_name("watch")
                    .short("w")
                    .help("Watch INPUT_FILE for changes")
            )
            .arg(
                Arg::with_name("stylesheet")
                    .short("css")
                    .help("CSS file tcsso include")
                    .takes_value(true)
            )
            .get_matches();

    let input = matches.value_of("INPUT_FILE").unwrap();
    let css = matches.value_of("stylesheet").unwrap_or("styles.css");

    if matches.is_present("watch") {
        watch(input, css);
    } else {
        convert(input, css);
    }
}

fn convert(path: &str, css_path: &str) {
    let src = fs::get_source(path);

    let doc = Document::new(src);

    fs::write_output(&doc.to_html(), css_path);
}
fn watch(path: &str, css_path: &str) {
    let (tx, rx) = channel();
    let mut watcher = watcher(tx, core::time::Duration::from_millis(10)).unwrap();
    watcher.watch(path, notify::RecursiveMode::Recursive).unwrap();
    watcher.watch(css_path, notify::RecursiveMode::Recursive).unwrap();

    loop {
        match rx.recv() {
           Ok(notify::DebouncedEvent::Write(_)) => convert(path, css_path),
           Err(e) => println!("watch error: {:?}", e),
           _ => {}
        }
    } 

}
