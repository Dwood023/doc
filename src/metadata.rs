use node::{Node, Tag};
pub struct Author {
    pub name: String,
    pub email: Option<String>
}
pub struct Metadata {
    pub title: String,
    pub author: Option<Author>,
}

impl Metadata {
    pub fn to_html(self) -> Node {

        let mut children = vec!(
            Node::Parent {
                tag: Tag::Header { level: 1 },
                children: vec!(
                    Node::Leaf(self.title)
                )
            }
        );

        if let Some(author) = self.author {
            children.push(
                Node::Leaf(author.name)
            )
        }

        Node::Parent {
            tag: Tag::Div { 
                classes: Some("header".to_owned())
            },
            children
        }
    }
}