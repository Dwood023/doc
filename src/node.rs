use itertools::Itertools;

#[derive(Debug)]
pub enum Node {
    Parent {
        tag: Tag, // TODO -- enum for BOLD, PARAGRAPH etc.
        children: Vec<Node>,
    },
    Leaf(String),
}

#[derive(Debug, Clone)]
pub enum Tag {
    Div {
        classes: Option<String>
    },
    Header {
        level: usize
    },
    Link {
        to: String,
        from: String
    },
    Image {
        data: String
    },
    Paragraph,
    Bold,
    Italic,
    Superscript,
    Subscript,
    StrikeThrough,
    Body,
    ListItem,
    OrderedList,
    UnorderedList
}

impl Tag {
    pub fn to_string(&self) -> &'static str {
        use self::Tag::*;
        match self {
            Header {level} => match level {
                1 => "h1",
                2 => "h2",
                3 => "h3",
                4 => "h4",
                5 => "h5",
                _ => "h6",
            },
            Paragraph => "p",
            Div {..} => "div",
            Bold => "b",
            Italic => "em",
            Superscript => "sup",
            Subscript => "sub",
            Link {..} => "a",
            Body => "body",
            ListItem => "li",
            OrderedList => "ol",
            UnorderedList => "ul",
            StrikeThrough => "s",
            Image {..} => "img"
        }
    }
    pub fn serialize(&self, children: &Vec<Node>) -> String {

        let tag = self.to_string();
        let children = children.into_iter()
            .map(|child| child.serialize())
            .join("");

        match self {
            Tag::Div { classes } => {
                let classes = match classes {
                    Some(classes) => &classes,
                    None => ""
                };
                format!("<{} class='{}'>{}</{}>", tag, classes, children, tag)
            },
            Tag::Link { to, from } => {
                format!("<{} id='{}' href='#{}'>{}</{}>", tag, from, to, children, tag)
            },
            Tag::Image { data } => {
                format!("<{} src='{}'>", tag, data)
            },
            _ => format!("<{}>{}</{}>", tag, children, tag)
        }
    }
}

impl Node {
    pub fn serialize(&self) -> String {
        use self::Node::{Parent, Leaf};
        match self {
            Parent {tag, children} => tag.serialize(children),
            Leaf(c) => c.to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Node::*;
    use super::Tag::*;

    #[test]
    fn ser() {
        let example = Parent {
            tag: Body,
            children: vec!(
                Leaf("f".to_owned()),
                Parent {
                    tag: Bold,
                    children: vec!(
                        Leaf("UUU".to_owned())
                    )
                },
                Leaf("ck".to_owned())
            )
        };

        println!("{}", example.serialize());
    }
}