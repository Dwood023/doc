use std::collections::VecDeque;
use std::iter;
use itertools::Itertools;

use node::{Node, Tag};
use line::Content::{self, *};
use regex::Regex;
use utils::PopWhile;
use footnotes::Footnotes;
use cursor::Cursor;
use document::Document;
use metadata::{Metadata, Author};

pub struct Parser {
    pub footnotes: Footnotes
}
impl Parser {
    pub fn new() -> Parser {
        Parser {
            footnotes: Footnotes::new()
        }
    }
    pub fn parse_document(mut self, lines: Vec<Option<Content>>) -> Document {

        let mut doc: VecDeque<_> = lines.into_iter().collect();
        let mut metadata = None;

        // If the first line is an h1, get adjacent lines for header
        let has_metadata = |doc: &VecDeque<Option<Content>>| match doc.get(0) {
            Some(Some(Header { level: 1, .. })) => true,
            _ => false
        };

        if has_metadata(&doc) {
            let is_metadata = |l: &Option<Content>| l.is_some();
            metadata = Some(
                self.parse_metadata(doc.pop_while(is_metadata))
            );
        }

        let body = self.parse_body(doc);

        Document {
            metadata,
            body,
            footnotes: self.footnotes
        }

    }

    pub fn parse_body(&mut self, mut lines: VecDeque<Option<Content>>) -> Vec<Node> {

        let mut body: Vec<Node> = vec!();

        while let Some(x) = lines.pop_front() {
            
            if let Some(content) = x {
                body.push(self.parse_line(content, &mut lines))
            }
        }
        body
    }
    /*
        Section at the top of document

        = Document title
        author: David Wood <Dwood023@live.co.uk>
        date: 12/04/19
    */
    fn parse_metadata(&mut self, mut header_lines: VecDeque<Option<Content>>) -> Metadata {
        lazy_static! {
            // Modifiers - Wraps content
            static ref AUTHOR: Regex = Regex::new(r"(?U)^(?P<name>(\w+ )+)\s*<(?P<email>.+)>").unwrap();

        }
        let mut author = None;
        let mut title = String::new();
        while let Some(Some(content)) = header_lines.pop_front() {
            match content {
                Header { content, ..} => title = content.to_owned(),
                Paragraph {content, .. } => {
                    if let Some(cap) = AUTHOR.captures(content) {

                        author = Some(
                            Author {
                                name: cap.name("name").unwrap().as_str().to_owned(),
                                email: cap.name("email").map(|c| c.as_str().to_owned())

                        })
                    }
                },
                _ => {}
            }
        }
        Metadata {
            title,
            author
        }
    }

    fn parse_line(&mut self, current: Content, remaining: &mut VecDeque<Option<Content>>) -> Node {

        const BREAKS_ALLOWED_BETWEEN_CHILDREN: bool = true;

        let is_child = |l: &Option<Content>| match l {
            Some(content) => current.is_parent_of(content),
            None => BREAKS_ALLOWED_BETWEEN_CHILDREN
        };

        match current {
            Paragraph { content, .. } => {

                let is_p = |l: &Option<Content>| match l {
                    Some(Paragraph {..}) => true,
                    _ => false
                };

                let adj = remaining
                    .pop_while(is_p)
                    .into_iter()
                    .filter_map(
                        |l| match l {
                            Some(Paragraph {content, ..}) => Some(content),
                            _ => None
                        }
                    );

                let content = iter::once(content)
                    .chain(adj)
                    .join(" ");

                Node::Parent {
                    tag: current.get_tag(),
                    children: self.inline_parse(&content)
                }
            },
            ListItem { variant: first_type, indents: first_indents, .. } => {
                let is_sibling = |l: &Option<Content>| match l {
                    Some(ListItem {indents, ..}) => *indents == first_indents,
                    _ => false 
                };

                let should_include = |l: &Option<Content>|
                    is_sibling(l) || is_child(l);
                
                // This clone is unfortunate, but processing current with
                // its siblings is convenient
                let mut list: VecDeque<_> = iter::once(Some(current.clone()))
                    .chain(remaining.pop_while(should_include))
                    .into_iter().collect();

                let mut list_content: Vec<Node> = vec!();
                
                while let Some(list_member) = list.pop_front() {
                    // Sibling
                    if let Some(ListItem { content, ..}) = list_member {
                        let this = self.inline_parse(&content);
                        // Get siblings children
                        let children = self.parse_body(list.pop_while(is_child));

                        list_content.push(
                            Node::Parent {
                                tag: Tag::ListItem,
                                children: this.into_iter()
                                    .chain(children)
                                    .collect()
                            }
                        )
                    }
                }

                Node::Parent {
                    tag: first_type.get_list_tag(),
                    children: list_content
                }
            },
            Div {..} => {
                Node::Parent {
                    tag: current.get_tag(),
                    children: self.parse_body(remaining.pop_while(is_child))
                }
            },
            Header { content, ..} => {
                Node::Parent {
                    tag: current.get_tag(),
                    children: self.inline_parse(&content)
                }
            }
        }
    }

    pub fn inline_parse(&mut self, content: &str) -> Vec<Node> {
        lazy_static! {
            // Modifiers - Wraps content
            static ref BOLD: Regex = Regex::new(r"(?U)^\*(?P<content>.+)\*").unwrap();
            static ref ITALIC: Regex = Regex::new(r"(?U)^~(?P<content>.+)~").unwrap();
            static ref SUPER: Regex = Regex::new(r"(?U)^\^(?P<content>.+)\^").unwrap();
            static ref SUB: Regex = Regex::new(r"(?U)^_(?P<content>.+)_").unwrap();
            static ref STRIKE: Regex = Regex::new(r"(?U)^--(?P<content>.+)--").unwrap();
            static ref LITERAL: Regex = Regex::new(r"(?U)^`(?P<content>.+)`").unwrap();
            // Macros - Generates content
            static ref FOOTNOTE: Regex = Regex::new(r"^footnote:\[(?P<content>.*)\]").unwrap();
            static ref IMAGE: Regex = Regex::new(r"^image:(?P<url>.*)\[(?P<title>.*)\]").unwrap();
        }
        use node::Tag::*;

        let mut output: Vec<Node> = vec!();

        let mut cursor = Cursor::new(content);

        while !cursor.is_empty() {

            fn extract_content<'a>(cap: regex::Captures<'a>) -> &'a str {
                cap.name("content").unwrap().as_str()
            }
            fn ctor(parser: &mut Parser, tag: Tag, cap: regex::Captures) -> Node {
                Node::Parent {
                    tag,
                    children: parser.inline_parse(extract_content(cap))
                }
            }

            output.push(
                if let Some(cap) = cursor.detect(&BOLD) {
                    ctor(self, Bold, cap)
                }
                else if let Some(cap) = cursor.detect(&ITALIC) {
                    ctor(self, Italic, cap)
                }
                else if let Some(cap) = cursor.detect(&SUPER) {
                    ctor(self, Superscript, cap)
                }
                else if let Some(cap) = cursor.detect(&SUB) {
                    ctor(self, Subscript, cap)
                }
                else if let Some(cap) = cursor.detect(&STRIKE) {
                    ctor(self, StrikeThrough, cap)
                }
                else if let Some(cap) = cursor.detect(&LITERAL) {
                    Node::Leaf(
                        htmlescape::encode_minimal(
                            extract_content(cap)
                        )
                    )
                }
                else if let Some(cap) = cursor.detect(&FOOTNOTE) {
                    self.footnotes.generate_footnote(extract_content(cap))
                }
                else if let Some(cap) = cursor.detect(&IMAGE) {
                    let url = cap.name("url").unwrap().as_str();
                    let data = image_base64::to_base64(url);

                    Node::Parent {
                        tag: Image { data },
                        children: vec!()
                    }
                }
                else {
                    Node::Leaf(cursor.take_one().to_owned())
                }
            )
        }

        let merged_leaves = output.into_iter().coalesce( |x, y| {
                if let (Node::Leaf(x), Node::Leaf(y)) = (&x, &y) {
                    return Ok(Node::Leaf(format!("{}{}", x, y)))
                }
                Err((x,y))
            }
        ).collect();

        return merged_leaves
    }

}