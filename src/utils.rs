use std::collections::VecDeque;
/*
    Pops every element of vec which contiguously matches the predicate, or none if no contiguous matches are found.
*/
pub trait PopWhile<T> {
    fn pop_while<F>(&mut self, predicate: F) -> VecDeque<T>
    where F: Fn(&T) -> bool; 
}
impl<T> PopWhile<T> for VecDeque<T> {
    fn pop_while<F>(&mut self, predicate: F) -> VecDeque<T>
    where F: Fn(&T) -> bool {

        let mut output: VecDeque<T> = VecDeque::new();
        let mut pop_count = 0;

        for e in self.iter() {
            match predicate(e) {
                true => pop_count += 1,
                false => break
            }
        }

        for _ in 0..pop_count {
            output.push_back(self.pop_front().unwrap());
        }

        output
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn adj() {
        let vec = vec!(3, 1, 6, 3, 3, 6, 2, 3, 1);
        let mut input: VecDeque<_> = vec.iter().collect(); 
        println!("{:?}", input.pop_while(|&&x| x % 3 == 0));
    }
    #[test]
    fn adj2() {
        let vec = vec!("a", "b", "a", "g", "a");
        let mut input: VecDeque<_> = vec.iter().collect();
        println!("{:?}", input.pop_while(|&&x| x == "a" || x == "b"));
    }
}